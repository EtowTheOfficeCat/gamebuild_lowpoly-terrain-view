# LowPoly Terrain View

**Hello there! EtowTheOfficeCat here! presenting you today a small but beautifull LowPoly creation**

![](https://media.giphy.com/media/pOKrXLf9N5g76/giphy.gif)

This is an older  project I re-uploaded. I created it to learn more about and mostly refine my skills with unity graphics/post processing as well as the HD Render Pipeline and simple unity/blender animations. For that purpouse I created this little LowPoly environment. I created all the assets in this project myself, you can find them on my [(Artstation)](https://www.artstation.com/paulmabon/albums/all)

**GameBuild Download**

https://www.dropbox.com/s/cddmyckvfhny759/Terrain%20View.zip?dl=0

**Video Preview:**

https://youtu.be/4g22WYASfms


**Pictures:**

![Alt Text](https://i.imgur.com/4IiSDxc.png)

![Alt Text](https://i.imgur.com/5tLpvlR.png)

Used Unity / 3DS max / Blender and Substance Painter for this project